<?php

/**
 * @file
 * Admin page callbacks for the redmine_issues module.
 */

/**
 * Form constructor for the redmine_issues configuration form.
 *
 * @ingroup forms
 */
function redmine_issues_admin_configure() {
  $form = array();
  $form['redmine_issues_project_identifier'] = array(
    '#title' => t('Redmine project identifier'),
    '#description' => t('The last part of the URL of the project in Redmine, for example enter "foo" if the Redmine URL for this support project is http://your.redmine.com/project/foo.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('redmine_issues_project_identifier', ''),
    '#required' => TRUE,
  );

  // Load available tracker types from Redmine.
  $options = array();
  $fetch = 'trackers';
  $result = redmine_rest_api_call($fetch);
  if ($result->decoded_data) {
    $options[] = '-- Select tracker --';
    foreach ($result->decoded_data->trackers as $tracker) {
      $options[$tracker->id] = $tracker->name;
    }
  }
  // Warn if there is no Redmine link.
  else {
    drupal_set_message(t('There is a problem with the Redmine link and we cannot load tracker information. See the Drupal log for more information.'), 'error');
  }
  $form['redmine_issues_target_tracker'] = array(
    '#title' => t('Target tracker'),
    '#description' => t('The tracker type in Redmine you want Drupal to use for all new issues. This is also the only tracker shown on the issues page.'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => variable_get('redmine_issues_target_tracker', ''),
    '#required' => TRUE,
  );

  // Load available status types from Redmine.
  $options = array();
  $fetch = 'issue_statuses';
  $result = redmine_rest_api_call($fetch);
  if ($result->decoded_data) {
    $options[] = '-- Select status --';
    foreach ($result->decoded_data->issue_statuses as $status) {
      $options[$status->id] = $status->name;
    }
  }
  // Warn if there is no Redmine link.
  else {
    drupal_set_message(t('There is a problem with the Redmine link and we cannot load status information. See the Drupal log for more information.'), 'error');
  }
  $form['redmine_issues_resolved_status'] = array(
    '#title' => t('Resolved status'),
    '#description' => t('The status in Redmine you want Drupal to use to mark issues as resolved.'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => variable_get('redmine_issues_resolved_status', ''),
    '#required' => TRUE,
  );
  $form['redmine_issues_reopened_status'] = array(
    '#title' => t('Re-opened status'),
    '#description' => t('The status in Redmine you want Drupal to use to mark issues as re-opened with.'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => variable_get('redmine_issues_reopened_status', ''),
    '#required' => TRUE,
  );

  $options = array();
  $formats = filter_formats();
  $options[0] = '-- ' . t('Select') . ' --';
  foreach ($formats as $format => $format_info) {
    $options[$format] = $format_info->name;
  }
  $form['redmine_issues_input_format'] = array(
    '#title' => t('Redmine input format'),
    '#description' => t('The input format to filter Redmine issues with so they display correctly.'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => variable_get('redmine_issues_input_format', ''),
  );

  return system_settings_form($form);
}
