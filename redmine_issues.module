<?php

/**
 * @file
 * Provides a UI to manage issue queues in Redmine from Drupal.
 */

/**
 * Implements hook_permission().
 */
function redmine_issues_permission() {
  return array(
    'manage redmine issues' => array(
      'title' => t('Manage Redmine issues'),
      'description' => t('Access, create, update and close Redmine issues.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function redmine_issues_menu() {
  // Issue list.
  $items['issues'] = array(
    'title' => t('Your issues'),
    'description' => t('View a list of issues for your project.'),
    'page callback' => 'redmine_issues_list_issues',
    'access arguments' => array('manage redmine issues'),
  );
  $items['issue'] = array(
    'title' => t('View issue'),
    'description' => t('View an individual issue.'),
    'page callback' => 'redmine_issues_view_issue',
    'page arguments' => array(1),
    'access arguments' => array('manage redmine issues'),
    'type' => MENU_CALLBACK,
  );
  $items['issues/new'] = array(
    'title' => t('New issue'),
    'description' => t('Create a new issue.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('redmine_issues_new_issue'),
    'access arguments' => array('manage redmine issues'),
  );
  // Settings.
  $items['admin/config/redmine/redmine-issues'] = array(
    'title' => t('Redmine project settings'),
    'description' => 'Configure the settings for your Redmine project.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('redmine_issues_admin_configure'),
    'access arguments' => array('administer redmine'),
    'file' => 'redmine_issues.admin.inc',
  );
 return $items;
}

/**
 * Form showing the update field to append information to an
 * existing Redmine issue.
 *
 * @param int $issueid
 *  Redmine issue ID.
 *
 * @param int $issuestatusid
 *  Current status of Redmine by status ID.
 *
 * @see redmine_issues_update_issue_submit
 * @ingroup forms
 */
function redmine_issues_update_issue($form, &$form_state, $vars) {
  $form = array();
  $form['redmine_issues_issue_updated'] = array(
    '#title' => t('Add a comment'),
    '#description' => t('Leave additional information relevant to this issue using this form.'),
    '#type' => 'textarea',
  );
  $form['redmine_issues_update_upload'] = array(
    '#type' => 'file',
    '#title' => t('Attach a file'),
    '#description' => t('Optional, larger files may be rejected.'),
  );
  // Check the current issue status and set default value.
  if ($vars['issuestatusid'] == variable_get('redmine_issues_resolved_status', '')) {
    $form['redmine_issues_issue_reopen'] = array(
      '#type' => 'checkbox',
      '#title' => t('Re-open'),
      '#description' => t('Issue currently marked as resolved, check the box and click "Post comment" to re-open this issue.'),
    );
  }
  else {
    $form['redmine_issues_issue_resolved'] = array(
      '#type' => 'checkbox',
      '#title' => t('Resolved'),
      '#description' => t('Check the box and click "Post comment" to mark this issue as resolved.'),
    );
  }
  $form['redmine_issues_issue_id'] = array(
    '#type' => 'value',
    '#value' => $vars['issueid'],
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Post comment'),
  );
  return $form;
}

/**
 * Validate function for capturing any uploaded files.
 */
function redmine_issues_update_issue_validate($form, &$form_state) {
  // Taken from:
  // https://api.drupal.org/api/examples/form_example%21form_example_tutorial.inc/function/form_example_tutorial_10_validate/7
  $file = file_save_upload('redmine_issues_update_upload');
  // Check the file saved OK.
  if ($file) {
    // Move the file, into the Drupal file system.
    if ($file = file_move($file, 'public://')) {
      // Save the file for use in the submit handler.
      $form_state['storage']['redmine_issues_update_upload'] = $file;
    }
    else {
      form_set_error('redmine_issues_update_upload', t('Failed to write the uploaded file to the site\'s file folder.'));
    }
  }
}

/**
 * Submit handler for posting issue updates to Redmine.
 *
 * @see redmine_issues_update_issue
 */
function redmine_issues_update_issue_submit($form, &$form_state) {
  // Get this user's username (should match Redmine login).
  global $user;
  $login = $user->name;

  // Handle file upload, if one exists.
  $upload_token = NULL;
  if (isset($form_state['storage']['redmine_issues_update_upload'])) {
    $uri = $form_state['storage']['redmine_issues_update_upload']->uri;
    $fetch = 'uploads';
    if (ob_get_level()) {
      ob_end_clean();
    }
    $scheme = file_uri_scheme($uri);
    $data = '';
    // Process file in 1024 byte chunks to save memory usage.
    if ($scheme && file_stream_wrapper_valid_scheme($scheme) && $fd = fopen($uri, 'rb')) {
      while (!feof($fd)) {
        $data .= fread($fd, 1024);
      }
      fclose($fd);
    }
    else {
      drupal_not_found();
    }
    $upload_result = redmine_rest_api_call($fetch, array(), 'POST', $data, 'json', $login);

    if (isset($upload_result->decoded_data->upload->token)) {
      $upload_token = $upload_result->decoded_data->upload->token;
      // Delete the file, now we've sent it to Redmine.
      drupal_unlink($uri);
    }
  }

  // Now save the actual issue update.

  // Build URL part to fetch.
  $fetch = 'issues/' . $form_state['build_info']['args'][0]['issueid'];

  // Build data string to send to Redmine.
  $update = '';
  if (isset($form_state['values']['redmine_issues_issue_updated'])) {
    $update = $form_state['values']['redmine_issues_issue_updated'];
  }

  // If we processed an upload, build the XML envelope for it.
  $upload = '';
  if ($upload_token) {
    $filename = $form_state['storage']['redmine_issues_update_upload']->filename;
    $upload = "
    <uploads type=\"array\">
    <upload>
    <token>$upload_token</token>
    <filename>$filename</filename>
    </upload>
    </uploads>";
  }

  // Build status for issue.
  $status = '';
  if (isset($form_state['values']['redmine_issues_issue_resolved']) || isset($form_state['values']['redmine_issues_issue_reopen'])) {
    // Check field exists first *and* then if it is checked.
    if (isset($form_state['values']['redmine_issues_issue_resolved']) && $form_state['values']['redmine_issues_issue_resolved']) {
      $status = variable_get('redmine_issues_resolved_status', '');
    }
    // Check field exists first *and* then if it is checked.
    elseif (isset($form_state['values']['redmine_issues_issue_reopen']) && $form_state['values']['redmine_issues_issue_reopen']) {
      $status = variable_get('redmine_issues_reopened_status', '');
    }

    // Build XML envelope and contents.
    $data = "<?xml version=\"1.0\"?>
<issue>
  <notes>$update</notes>
  <status_id>$status</status_id>$upload
</issue>";
  }
  else {
    $data = "<?xml version=\"1.0\"?>
<issue>
  <notes>$update</notes>$upload
</issue>";
  }

  // Make the API call.
  $result = redmine_rest_api_call($fetch, array(), 'PUT', $data, 'xml', $login);
  // Check the response code to make sure it worked.
  if ($result->code == '200') {
    drupal_set_message(t('Issue updated.'));
  }
  else {
    drupal_set_message(t('There was a problem updating your issue.'), 'error');
  }
}

/**
 * Form showing the fields required to create a new Redmine issue.
 *
 * @see redmine_issues_new_issue_submit
 * @ingroup forms
 */
function redmine_issues_new_issue() {
  $form = array();
  $form['redmine_issues_new_issue_title'] = array(
    '#title' => t('Issue title'),
    '#description' => t('Please enter a descriptive title for your issue.'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['redmine_issues_new_issue_description'] = array(
    '#title' => t('Issue description'),
    '#description' => t('Please describe, in as much detail as you can, the problem you are experiencing and the steps we can take to reproduce it, including extra information where relevant, such as Internet browser used.'),
    '#type' => 'textarea',
    '#required' => TRUE,
  );
  $form['redmine_issues_new_upload'] = array(
    '#type' => 'file',
    '#title' => t('Attach a file'),
    '#description' => t('Optional, larger files may be rejected.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create issue'),
  );
  return $form;
}

/**
 * Validate function for capturing any uploaded files.
 */
function redmine_issues_new_issue_validate($form, &$form_state) {
  // Taken from:
  // https://api.drupal.org/api/examples/form_example%21form_example_tutorial.inc/function/form_example_tutorial_10_validate/7
  $file = file_save_upload('redmine_issues_new_upload');
  // Check the file saved OK.
  if ($file) {
    // Move the file, into the Drupal file system.
    if ($file = file_move($file, 'public://')) {
      // Save the file for use in the submit handler.
      $form_state['storage']['redmine_issues_new_upload'] = $file;
    }
    else {
      form_set_error('redmine_issues_new_upload', t('Failed to write the uploaded file to the site\'s file folder.'));
    }
  }
}

/**
 * Submit handler for the new issue form, responsible for creating
 * the new issue in Redmine, based on the data submitted to Drupal.
 *
 * @see redmine_issues_new_issue
 */
function redmine_issues_new_issue_submit($form, &$form_state) {
  // Get this user's username (should match Redmine login).
  global $user;
  $login = $user->name;

  // Handle file upload, if one exists.
  // @TODO Below this line could be an API function in redmine_rest_api.module.
  $upload_token = NULL;
  if (isset($form_state['storage']['redmine_issues_new_upload'])) {
    $uri = $form_state['storage']['redmine_issues_new_upload']->uri;
    $fetch = 'uploads';
    if (ob_get_level()) {
      ob_end_clean();
    }
    $scheme = file_uri_scheme($uri);
    $data = '';
    // Process file in 1024 byte chunks to save memory usage.
    if ($scheme && file_stream_wrapper_valid_scheme($scheme) && $fd = fopen($uri, 'rb')) {
      while (!feof($fd)) {
        $data .= fread($fd, 1024);
      }
      fclose($fd);
    }
    else {
      drupal_not_found();
    }
    $upload_result = redmine_rest_api_call($fetch, array(), 'POST', $data, 'json', $login);

    if (isset($upload_result->decoded_data->upload->token)) {
      $upload_token = $upload_result->decoded_data->upload->token;
      // Delete the file, now we've sent it to Redmine.
      drupal_unlink($uri);
    }
  }
  // @TODO Above this line could be an API function in redmine_rest_api.module.

  // Now save the actual issue.

  // Build URL part to fetch.
  $fetch = 'issues';
  // Build data string to send to Redmine.
  $trackerid = variable_get('redmine_issues_target_tracker', '');
  $identifier = variable_get('redmine_issues_project_identifier', '');
  $subject = check_plain($form_state['values']['redmine_issues_new_issue_title']);
  $description = check_plain($form_state['values']['redmine_issues_new_issue_description']);
  // If we processed an upload, build the XML envelope for it.
  $upload = '';
  if ($upload_token) {
    $filename = $form_state['storage']['redmine_issues_new_upload']->filename;
    $upload = "
  <uploads type=\"array\">
    <upload>
      <token>$upload_token</token>
      <filename>$filename</filename>
    </upload>
  </uploads>";
  }
  // JSON API is broken for new issues, but XML seems OK.
  // See: http://www.redmine.org/issues/8951
  $data = "<?xml version=\"1.0\"?>
<issue>
  <project_id>$identifier</project_id>
  <tracker_id>$trackerid</tracker_id>
  <subject>$subject</subject>
  <description>$description</description>$upload
</issue>";

  // Try to post data to API.
  $result = redmine_rest_api_call($fetch, array(), 'POST', $data, 'xml', $login);

  if ($result->decoded_data) {
    drupal_set_message(t('Issue created.'));
    drupal_goto('issue/' . $result->decoded_data[1]['value']);
  }
  else {
    return t('No XML data received. Please check the data in the messages above carefully.');
  }
}

/**
 * Display a single Redmine issue on a Drupal page.
 */
function redmine_issues_view_issue($issueid) {
  // Build URL part to fetch.
  $fetch = 'issues/' . $issueid;
  // Build additional parameters.
  $params = array(
    // Make sure we load history and files.
    'include' => 'attachments,journals',
  );
  // Try to get JSON data.
  $result = redmine_rest_api_call($fetch, $params);

  $output = '';
  // $result->decoded_data will be NULL if there was no JSON data in returned HTTP result.
  if ($result->decoded_data) {
    $issue = $result->decoded_data->issue;
    drupal_set_title($issue->subject);

    $assignee = '';
    if (!empty($issue->assigned_to->name)) {
      $assignee = $issue->assigned_to->name;
    }
    else {
      $assignee = t('Nobody');
    }

    $attachments = '';
    if (empty($issue->attachments)) {
      $attachments = 'None';
    }
    else {
      foreach ($issue->attachments as $attachment) {
        $attachments .= l($attachment->filename, $attachment->content_url) . '<br />';
      }
    }

    // Create info block.
    $rows = array(
      array(
        t('Created by'),
        $issue->author->name,
      ),
      array(
        t('Created on'),
        format_date(strtotime($issue->created_on)),
      ),
      array(
        t('Last updated'),
        format_date(strtotime($issue->updated_on)),
      ),
      array(
        t('Spent hours'),
        $issue->spent_hours,
      ),
      array(
        t('Status'),
        $issue->status->name,
      ),
      array(
        t('Assigned to'),
        $assignee,
      ),
      array(
        t('Attachments'),
        $attachments,
      ),
    );
    $variables = array('rows' => $rows);
    $output .= theme('table', $variables);

    // @TODO: This should be a proper theme and template implementation.

    $output .= '<h2>' . t('Issue summary') . '</h2>';

    // Use the selected input format for filtering Redmine issue text.
    if (variable_get('redmine_issues_input_format', '')) {
      $output .= '<div>' . check_markup($issue->description, variable_get('redmine_issues_input_format', '')) . '</div>';
    }
    // Use default if none specified.
    else {
      $output .= '<div>' . check_markup($issue->description) . '</div>';
    }


    $output .= '<div class="notes">';
    $output .= '<h2>' . t('Comments') . '</h2>';
    foreach ($issue->journals as $journal) {
      if (isset($journal->notes)) {
        $output .= '<div class="note">';
        // Use the selected input format for filtering Redmine issue text.
        if (variable_get('redmine_issues_input_format', '')) {
          $output .= '<div>' . check_markup($journal->notes, variable_get('redmine_issues_input_format', '')) . '</div>';
        }
        // Use default if none specified.
        else {
          $output .= '<div>' . check_markup($journal->notes) . '</div>';
        }
        $output .= '<div class="name">' . t('By %user', array('%user' => $journal->user->name)) . '</div>';
        $output .= '<div>' . t('On %date', array('%date' => format_date(strtotime($journal->created_on)))) . '</div>';
        $output .= '<hr />';
        $output .= '</div>';
      }
    }
    $update_form = drupal_get_form('redmine_issues_update_issue', array('issueid' => $issue->id, 'issuestatusid' => $issue->status->id));
    $output .= drupal_render($update_form);
    $output .= '</div>';

    // Show the actual JSON result if the devel module is enabled.
    if (module_exists('devel')) {
      $output .= '<pre>' . print_r($result, TRUE) . '</pre>';
    }
    return $output;
  }
  else {
    return t('No JSON data received. Please check the data in the messages above carefully.');
  }
}

/**
 * Lists the issues in a Redmine project.
 */
function redmine_issues_list_issues() {
  // Load the project identifier.
  $identifier = variable_get('redmine_issues_project_identifier', '');

  if ($identifier) {
    // Build URL part to fetch.
    $fetch = 'issues';
    // If filter form has been submitted, use the params set there.
    $query = drupal_get_query_parameters();
    if (!empty($query)) {
      $params = $query;
      $params['project_id'] = $identifier;
      $params['limit'] = 100;
    }
    // Otherwise, set default param for product ID.
    else {
      $params = array(
        'project_id' => $identifier,
        'limit' => 100,
      );
    }

    // Try to get JSON data.
    $result = redmine_rest_api_call($fetch, $params);

    // $result->decoded_data will be NULL if there was no JSON data in returned HTTP result.
    if ($result->decoded_data) {
      $header = array(
        t('Subject'),
        t('Status'),
        t('Assigned to'),
      );
      // Build array of table rows for issues.
      foreach ($result->decoded_data->issues as $issue) {
        // We're actually only interested in the target tracker in Redmine.
        if ($issue->tracker->id == variable_get('redmine_issues_target_tracker', '')) {
          // Sometimes the assigned_to parameter is empty, so we need to check.
          $assignee = '';
          if (!empty($issue->assigned_to->name)) {
            $assignee = $issue->assigned_to->name;
          }
          // Build this row.
          $rows[] = array(
            // Link to the issue page from the subject.
            l($issue->subject, 'issue/' . $issue->id),
            $issue->status->name,
            $assignee,
          );
        }
      }

      $output = '';
      if (!empty($rows)) {
        $variables = array(
          'header' => $header,
          'rows' => $rows,
        );
        $filter_form = drupal_get_form('redmine_issues_list_filter');
        $output = drupal_render($filter_form);
        // @TODO: Still need pagination.
        $output .= theme('table', $variables);
      }
      elseif (!empty($query)) {
        $filter_form = drupal_get_form('redmine_issues_list_filter');
        $output = drupal_render($filter_form);
        $output .= t('No issues matching the selected criteria.');
      }
      else {
        $output = t('No issues at this time.') . ' ' . l(t('Create a new issue.'), 'issues/new');
      }

      // Show the actual JSON result if the devel module is enabled.
      if (module_exists('devel')) {
        $output .= '<pre>' . print_r($result, TRUE) . '</pre>';
      }
      return $output;
    }
    else {
      return t('No JSON data received. Please check the data in the messages above carefully.');
    }
  }
  else {
    drupal_set_message(t('No Redmine project identifier configured. Go to Configuration and set one to continue.'), 'error');
    return t('No project configured.');
  }
}

/**
 * Form for filtering on Redmine issue list.
 *
 * @see redmine_issues_list_filter_submit
 */
function redmine_issues_list_filter() {
  $query = drupal_get_query_parameters();
  $show_mine = FALSE;
  $default_status_value = '';
  if (!empty($query)) {
    $default_status_value = $query['status_id'];
    if (isset($query['author_id'])) {
      $show_mine = TRUE;
    }
  }
  $form = array();
  $form['redmine_issues_list_filter_status'] = array(
    '#title' => t('Issue status'),
    '#type' => 'select',
    // '*' fetches all issues, other options 'open' (default) and 'closed'.
    '#options' => array(
      'open' => 'Open',
      'closed' => 'Closed',
      '*' => 'All',
    ),
    '#default_value' => $default_status_value,
  );
  $form['redmine_issues_list_filter_mine'] = array(
    '#title' => t('Show my issues only'),
    '#type' => 'checkbox',
    '#default_value' => $show_mine,
  );
    $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  return $form;
}

/**
 * Submit handler for Redmine issue list filter form.
 *
 * @see redmine_issues_list_filter
 */
function redmine_issues_list_filter_submit($form, &$form_state) {
  global $user;
  $query = array();
  if ($form_state['values']['redmine_issues_list_filter_mine']) {
    $userid = redmine_rest_api_user_lookup_id($user->name);
    // Avoid filtering on a bad user ID - make sure one was returned.
    if ($userid) {
      $query['author_id'] = $userid;
    }
    else {
      drupal_set_message(t('Could not find your Redmine user, showing all issues instead.'));
    }
  }
  $query['status_id'] = $form_state['values']['redmine_issues_list_filter_status'];
  drupal_goto('issues', array('query' => $query));
}